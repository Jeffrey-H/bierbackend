<?php

namespace App\Http\Controllers;

use App\Models\Bier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Bier::orderBy('id')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'naam',
            'brouwer',
            'type',
            'gisting',
            'perc',
            'inkoop_prijs'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        return response()->json(Bier::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Bier::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Bier::find($id)->update($request->all())
            ? response()->json(['status' => 200, 'message' => 'updated!'])
            : response()->json(['status' => 401, 'message' => 'error']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(Bier::find($id)->delete());
    }
}
