<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bier extends Model
{
    public $timestamps = false;

    protected $table = "bier";

    protected $fillable = [
        'naam',
        'brouwer',
        'type',
        'gisting',
        'perc',
        'inkoop_prijs'
    ];
}
